﻿using HWsolid.processors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWsolid
{
    public class ITSpecialist : IITSpecialist
    {
        private Dictionary<String ,Processor> _processors;      
        public Processor GetProcessor(String processorModel)
        {
            switch (processorModel)
            {
                case "Intel Core":
                    return _processors["Intel Core"];

                case "AMD Ryzen":
                    return _processors["AMD Ryzen"];

                case "Qualcomm Snapdragon":
                    return _processors["Qualcomm Snapdragon"];

                default: return null;
            }
        }

        public Good PackTheGood(Processor processor)
        {
            Good good = new Good(processor, "Инструкция по эксплуатации.");
            return good;
        }

        public ITSpecialist()
        {
            _processors = new Dictionary<String, Processor>();
            _processors.Add("Intel Core", new Intel());
            _processors.Add("AMD Ryzen", new AMD());
            _processors.Add("Qualcomm Snapdragon", new Qualcomm());
        }
    }
}
