﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWsolid
{
    public interface IConsultant
    {
        String InputData { get; set; }
        Processor SelectedProcessor { get; set; }

        void GetDataFromClient();
        void MessageBaseInformation();
        void MessageChoiceProcessor();
        void MessageProcessorInformation(Processor processor);
        void MessageIncorrectData();
        void MessageBuyProcessor();

        void MessageSucccessTrade();
        void MessageErrorTrade();



    }
}
