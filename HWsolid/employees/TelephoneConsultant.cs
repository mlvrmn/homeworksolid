﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWsolid
{
    public class TelephoneConsultant : IConsultant
    {
        public String InputData { get; set; }
        public Processor SelectedProcessor { get; set; }

        public void GetDataFromClient()
        {
            InputData = Console.ReadLine();
        }
        public void MessageBaseInformation()
        {
            Console.WriteLine("Здравствуйте, я сотрудник, проводящий консультирование по телефону.");
        }
        public void MessageChoiceProcessor()
        {
            Console.WriteLine("Назовите модель процессора из предложенных : "
                + "Intel Core, AMD Ryzen, Qualcomm Snapdragon");
        }
        public void MessageBuyProcessor()
        {
            Console.WriteLine("Желаете ли вы преобрести данную модель?");
        }
        public void MessageProcessorInformation(Processor processor)
        {
            SelectedProcessor = processor;

            Console.WriteLine("Информация о выбранном процессоре : \n ");
            Console.WriteLine($"Производитель: {processor.Manufacturer}, \n " +
                              $"Модель: {processor.Model}, \n " +
                              $"Архитектура: {processor.Architecture} \n " +
                              $"Скорость: {processor.CoreSpeed}\n " +
                              $"Максимальная рабочая температура: {processor.MaxWorkTemperature}\n " +
                              $"Технология Hyper-threading: {processor.HyperThreading}\n ");
        }
        public void MessageIncorrectData()
        {
            Console.WriteLine("Данные введены неверно. Попробуйте еще раз.");
        }
        public void MessageSucccessTrade()
        {
            Console.WriteLine("Приедте в магазин, получите чек, заберите товар на складе по чеку.");
        }
        public void MessageErrorTrade()
        {
            Console.WriteLine("Очень жаль :)");
        }
    }
}
