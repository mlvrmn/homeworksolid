﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWsolid
{
    public interface IITSpecialist
    {
        Processor GetProcessor(String processorModel);
        Good PackTheGood(Processor processor);
    }
}
