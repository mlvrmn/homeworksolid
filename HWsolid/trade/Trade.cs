﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWsolid
{
    public class Trade
    {
        private IITSpecialist _iTSpecialist;
        private IConsultant _consultant;
        private Good _good;
        private Processor processor;

        public Trade(IITSpecialist iTSpecialist, IConsultant consultant)
        {
            this._iTSpecialist = iTSpecialist;
            this._consultant = consultant;
        }

        public void StartTrade()
        {
            _consultant.MessageBaseInformation();
            _consultant.MessageChoiceProcessor();
            _consultant.GetDataFromClient();

            while (processor == null)
            {
                processor = _iTSpecialist.GetProcessor(_consultant.InputData);

                if (processor == null)
                {
                    _consultant.MessageIncorrectData();
                    _consultant.GetDataFromClient();
                }
            }

            _consultant.MessageProcessorInformation(processor);

            _consultant.MessageBuyProcessor();
            _consultant.GetDataFromClient();

            if (_consultant.InputData == "да")
            {
                _good = _iTSpecialist.PackTheGood(_consultant.SelectedProcessor);
                _consultant.MessageSucccessTrade();
            }

            else _consultant.MessageErrorTrade();
        } 
    }
}
