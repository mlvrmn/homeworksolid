﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWsolid
{
    public abstract class Processor
    {
        public String Manufacturer { get; set; }
        public String Model { get; set; }
        public String Architecture { get; set; }
        public Int32 CoreSpeed { get; set; }
        public Int32 MaxWorkTemperature { get; set; }
        public String HyperThreading { get; set; } 

        public Processor(String manufacturer, String model, String architecture, Int32 coreSpeed, Int32 maxWorkTemperature, String hyperThreading)
        {
            this.Manufacturer = manufacturer;
            this.Model = model;
            this.Architecture = architecture;
            this.CoreSpeed = coreSpeed;
            this.MaxWorkTemperature = maxWorkTemperature;
            this.HyperThreading = hyperThreading;
        }
    }
}
