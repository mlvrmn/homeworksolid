﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWsolid
{
    public class Good
    {
        public Processor Processor { get; set; }
        public String InstructionManual { get; set; }

        public Good(Processor processor, String instructionManual)
        {
            this.Processor = processor;
            this.InstructionManual = instructionManual;
        }
    }
}