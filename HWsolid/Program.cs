﻿using HWsolid.processors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWsolid
{
    class Program
    {
        static void Main(string[] args)
        {

            IConsultant consultant = new TelephoneConsultant();

            IITSpecialist iTSpecialist = new ITSpecialist();

            Trade trade = new Trade(iTSpecialist, consultant);

            trade.StartTrade();
            
            Console.ReadKey();
        }
    }
}
